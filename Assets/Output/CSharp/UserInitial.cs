
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Luban;
using SimpleJSON;


namespace cfg
{
public sealed partial class UserInitial : Luban.BeanBase
{
    public UserInitial(JSONNode _buf) 
    {
        { var __json0 = _buf["items"]; if(!__json0.IsArray) { throw new SerializationException(); } Items = new System.Collections.Generic.List<Beans.Item_ItemDistribution>(__json0.Count); foreach(JSONNode __e0 in __json0.Children) { Beans.Item_ItemDistribution __v0;  { if(!__e0.IsObject) { throw new SerializationException(); }  __v0 = Beans.Item_ItemDistribution.DeserializeItem_ItemDistribution(__e0);  }  Items.Add(__v0); }   }
    }

    public static UserInitial DeserializeUserInitial(JSONNode _buf)
    {
        return new UserInitial(_buf);
    }

    /// <summary>
    /// 初始物品
    /// </summary>
    public readonly System.Collections.Generic.List<Beans.Item_ItemDistribution> Items;
   
    public const int __ID__ = -980623143;
    public override int GetTypeId() => __ID__;

    public  void ResolveRef(Tables tables)
    {
        foreach (var _e in Items) { _e?.ResolveRef(tables); }
    }

    public override string ToString()
    {
        return "{ "
        + "items:" + Luban.StringUtil.CollectionToString(Items) + ","
        + "}";
    }
}

}

