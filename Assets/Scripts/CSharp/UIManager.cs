using utility;
using UnityEngine;
using synthesis;

namespace MyUI
{
    public class UIManager : MonoBehaviour
    {
        public static InventoryManager inventoryManager;
        public MyCursor myCursor;


        void Awake()
        {
            inventoryManager = transform.Find("Canvas/Synthesis/Inventory").GetComponent<InventoryManager>();
        }

        /// <summary>
        /// 当抓起道具时
        /// </summary>
        /// <param name="item"></param>
        public void ItemToCursor(int _itemId)
        {
            Debug.Log(gameObject);
            foreach (var n in Utilities.tables.TbItem.DataMap)
            {
                if (n.Value.Id == _itemId)
                {
                    SynthesisManager.currentItem = Utilities.tables.TbItem.Get(n.Key);
                    inventoryManager.ChangeInventoryDict(SynthesisManager.currentItem, -1);

                    myCursor.GetComponent<MyCursor>().SetMyCursor(Resources.Load<Sprite>($"Images/Items/{SynthesisManager.currentItem.ImageNormal}"));
                    return;
                }
            }
        }
    }
}
