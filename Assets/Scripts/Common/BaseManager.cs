using System.Diagnostics;

public class BaseManager<T> where T : new()
{

    private static T instance;
    public static T GetInstance()
    {
        if (instance == null)
            instance = new T();
        else
        {
            Debug.Print("实例已存在");
        }
        return instance;
    }
}