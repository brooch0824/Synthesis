using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    protected virtual void Awake()
    {
        instance = this as T;
    }


    /// <summary>
    /// 外部调用实例
    /// </summary>
    /// <returns></returns>
    public static T GetInstance()
    {
        return instance;
    }

}
