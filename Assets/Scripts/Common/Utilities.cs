
using System;
using cfg;
using SimpleJSON;
using UnityEngine;

namespace utility
{
    public static class MyResources
    {
        public readonly static string _itemPath = "Images/Items/";
        public readonly static string _synthesisPath = "Images/Synthesis/";
    }
    public static class Utilities
    {
        public static Tables tables = new Tables(Loader);
        public static JSONNode Loader(string fileName)
        {
            var text = Resources.Load<TextAsset>($"Text/Json/{fileName}");
            return JSON.Parse(text.ToString());
        }
    }
}

namespace cfg
{
    public partial class Item : IComparable<Item>
    {

        public int CompareTo(Item other)
        {
            if (other == null)
            {
                return 1;
            }

            return Id - other.Id;
        }
    }
}
