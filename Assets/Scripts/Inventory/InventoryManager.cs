using System;
using System.Collections.Generic;
using cfg;
using Unity.VisualScripting;
using UnityEngine;

[Serializable]
public class InventoryManager : MonoBehaviour
{
    // public static GameObject inventory;
    [SerializeField] GameObject _itemPath;
    [SerializeField] GameObject m_itemPref;
    static Dictionary<Item, int> _inventoryDict = new Dictionary<Item, int>();

    void Awake()
    {
        // inventory = this.gameObject;
        _itemPath = _itemPath ? transform.Find("Items/Container").gameObject : _itemPath;
        m_itemPref = Resources.Load<GameObject>("Prefabs/Items/Item");
    }

    void Start()
    {
        UserDataToInventoryDict();
        RefreshInventoryByInventoryDict();
    }

    /// <summary>
    /// 改变背包字典(自带刷新背包)
    /// </summary>
    /// <param name="data"></param>
    /// <param name="number"></param> <summary>
    public void ChangeInventoryDict(Item data, int number)
    {

        if (data == null) return;
        //如果背包列表有这个物品
        if (_inventoryDict.ContainsKey(data.ConvertTo<Item>()))
        {
            _inventoryDict[data.ConvertTo<Item>()] += number;
        }
        //UserDataToInventoryDict();我为啥要把玩家数据同步过来？同步只发送在熔炼就可以了
        RefreshInventoryByInventoryDict();
    }

    /// <summary>
    /// 背包字典同步用户数据
    /// </summary>
    public void UserDataToInventoryDict()
    {
        _inventoryDict = UserData.UserItems;
    }

    //=======重要=======
    //这个还是慎用吧，毕竟是重刷玩家的数据，之后还是用一个扣除列表去遍历扣除比较好
    //=======重要=======
    public void InventoryDictToUserData()
    {
        UserData.ChangeUserData(_inventoryDict);
    }




    /// <summary>
    /// 刷新背包列表
    /// </summary>
    public void RefreshInventoryByInventoryDict()
    {
        //清空背包
        //_inventoryDict.Clear();
        //清空物品生成地址里的每一项
        foreach (Transform child in _itemPath.transform)
        {
            //Debug.Log("即将销毁的是" + child.name);
            Destroy(child.gameObject);
        }
        //根据用户的物品数据重新生成物品
        foreach (var i in _inventoryDict)
        {
            if (i.Value > 0) CreateItem(i.Key);
        }
        Debug.Log("刷新背包成功");
    }

    /// <summary>
    /// 创建背包中的物品
    /// </summary>
    /// <param name="item"></param>
    private void CreateItem(Item item)
    {
        // Debug.Log("预计发放：" + item.TextName);
        GameObject newItem = Instantiate(m_itemPref, _itemPath.transform);
        newItem.GetComponent<MyItem>().OnInstance(item);
        // Debug.Log("已经发放：" + item.TextName);
        // newItem.GetComponent<MyItem>().item = item;
    }

}