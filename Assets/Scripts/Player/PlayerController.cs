using UnityEngine;


public class PlayerController : MonoBehaviour
{
    private static PlayerController instance;
    public static PlayerController GetInstance()
    {
        return instance;
    }
    
    public PlayerInput playerInput;
    [Range(0, 10)] public float moveMulty = 1;

    private void Awake()
    {
        //实例化我们刚刚生成的rpgInputActions脚本
        playerInput = new PlayerInput();
        instance = this;
    }
    void OnEnable()
    {
        //使用前需要将该rpgInputActions开启
        playerInput.Keyboard.Enable();
    }
    void OnDisable()
    {
        //使用完需要将该rpgInputActions关闭
        playerInput.Keyboard.Disable();
    }
    //Update生命周期函数
    private void Update()
    {
        getMoveInput();
    }



    private void getMoveInput()
    {
        //将读取到的Move返回值赋值给moveVector2 
        Vector2 moveVector2 = playerInput.Keyboard.moveController.ReadValue<Vector2>();
        //因为我们的playerMove会在Update生命周期函数中逐帧执行，所以在执行前需要判断是否有按下对应的按键
        if (moveVector2 != Vector2.zero)
        {
            //使用获取到的Vector2.x和Vector2.y返回值作为角色移动的参数
            playerMove(moveVector2.x, moveVector2.y);
        }
    }
    //使角色真正移动的方法
    private void playerMove(float horizontal, float vertical)
    {
        transform.Translate(new Vector3(horizontal, 0, vertical) * Time.deltaTime * moveMulty, Space.World);
    }
}


