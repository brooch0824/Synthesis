using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MyCursor : MonoBehaviour
{
    // Update is called once per frame
    public GameObject m_Cursor;
    public Sprite normalCursor;
    public bool isNormal;
    public Animator m_CursorAnim;

    void Awake()
    {
        m_Cursor = transform.Find("Canvas/Cursor").gameObject;
        m_CursorAnim = transform.GetComponent<Animator>();
    }

    void Start()
    {
        Cursor.visible = false;
        SetMyCursor(null);
    }
    void Update()
    {
        m_Cursor.transform.position = Input.mousePosition;
        CheckMyCursorAnim();
    }

    void CheckMyCursorAnim()
    {
        m_CursorAnim.enabled = isNormal;
    }

    public void SetMyCursor(Sprite sprite)
    {
        if (sprite == null)//视为恢复默认指针
        {
            isNormal = true;
            CheckMyCursorAnim();
            m_Cursor.GetComponent<Image>().sprite = normalCursor;
            SetMyCursorNativeSize();
            Debug.Log("SetMyCursorNormal");
        }
        else
        {
            isNormal = false;
            CheckMyCursorAnim();
            m_Cursor.GetComponent<Image>().sprite = sprite;
            SetMyCursorNativeSize();
        }
    }


    void SetMyCursorNativeSize()
    {
        m_Cursor.GetComponent<Image>().SetNativeSize();
    }


}
