using System;
using cfg;
using UnityEngine;
using UnityEngine.UI;
using utility;
using MyUI;

[Serializable]
public class MyItem : MonoBehaviour
{
    public Item item;
    [SerializeField] private int _itemId;
    [SerializeField] private Text _itemNamePath;
    [SerializeField] private Image _itemImagePath;
    [SerializeField] private Text _itemCountPath;
    [SerializeField] private GameObject _itemInfo;
    void Awake()
    {
        _itemNamePath = transform.Find("BackGround/Text").GetComponent<Text>();
        _itemImagePath = transform.Find("BackGround/Image").GetComponent<Image>();
        _itemCountPath = transform.Find("BackGround/Count").GetComponent<Text>();
        _itemInfo = transform.Find("BackGround/Info").gameObject;
        _itemInfo.SetActive(false);

    }

    public void OnInstance(Item whoami)
    {
        //告诉实例它是谁
        _itemId = whoami.Id;
        foreach (Item i in Utilities.tables.TbItem.DataList)
        {
            //遍历找到本ID的物品ID
            if (_itemId == i.Id)
            {
                _itemNamePath.text = i.TextName;
                _itemImagePath.sprite = Resources.Load<Sprite>($"Images/Items/{i.ImageNormal}");
                _itemInfo.transform.Find("TextDesc").GetComponent<Text>().text = i.TextDesc;
                if (_itemImagePath.sprite == null) Debug.LogError($"【{i.TextName}】图片没找到");
            }
        }

        foreach (var i in UserData.UserItems)
        {
            if (i.Key == whoami)
            {
                _itemCountPath.text = i.Value.ToString();
            }
        }
    }

    public void OnClick()
    {
        _itemInfo.SetActive(false);
        _itemInfo.SetActive(true);
        transform.GetComponent<Animator>().Play("Item_Info_Common");
    }

    public void OnDrag(string methodName)
    {
        SendMessageUpwards(methodName, _itemId);
    }


    public void HideGameObject(string objectPath)
    {
        GameObject gameObject = transform.Find(objectPath).gameObject;
        gameObject.SetActive(false);
    }

}

