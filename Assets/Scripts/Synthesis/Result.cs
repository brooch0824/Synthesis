using System.Collections;
using cfg;
using MyUI;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Result : MonoBehaviour
{
    public Item ThisItem { get; set; }
    [SerializeField] SpriteRenderer _resultSprite;
    bool canChangeResultSprite = true;
    string _nullResultPath;
    ParticleSystem _particle;

    void Awake()
    {
        _particle = transform.Find("Image/Particle").GetComponent<ParticleSystem>();
        _nullResultPath = utility.MyResources._synthesisPath + "Synthesis_NullResult";
        _resultSprite = transform.Find("Image").GetComponent<SpriteRenderer>();
    }
    void Start()
    {
        _particle.Stop();

    }

    public void PreshowSynResult(Item targetItem)
    {
        ThisItem = targetItem;
        if (ThisItem == null)
        {
            ChangeSprite(_nullResultPath);
            return;
        }
        else
        {
            _particle.Stop();
            ChangeSprite(0.5f);
            ChangeSprite(utility.MyResources._itemPath + ThisItem.ImageNormal);
            Debug.Log("success PreShow");
        }
    }
    public void ShowSynResult(Item targetItem)
    {
        ThisItem = targetItem;
        Debug.Log("接收到的物体信息：" + ThisItem?.TextName);
        if (ThisItem != null)
        {
            StartCoroutine(GoShowSynResult());
        }
        else
        {
            StartCoroutine(WhereIsMySynResult());
            Debug.Log("success Over WhereIsMySynResult");
        }
    }

    void ChangeSprite(string path)
    {
        if (canChangeResultSprite == false) return;
        _resultSprite.sprite = Resources.Load<Sprite>(path);
        Debug.Log($"ChangePath:{path}");
    }

    void ChangeSprite(float colorC)
    {
        var color = _resultSprite.color;
        _resultSprite.color = new Color(color.r, color.g, color.b, colorC);
    }

    IEnumerator GoShowSynResult()
    {
        canChangeResultSprite = false;
        var old = ThisItem;
        UIManager.inventoryManager.ChangeInventoryDict(old, -1);
        ChangeSprite(1f);
        ChangeSprite(utility.MyResources._itemPath + ThisItem.ImageNormal);

        _particle.Stop();
        _particle.Play();

        Debug.Log("success GoShowSynResult");
        yield return new WaitForSeconds(3);
        UIManager.inventoryManager.ChangeInventoryDict(old, 1);
        //ThisItem = null;//==重要==这样不知道如果
        PreshowSynResult(ThisItem);
        Debug.Log("success changeback");
        canChangeResultSprite = true;
    }
    IEnumerator WhereIsMySynResult()
    {
        ChangeSprite(_nullResultPath);
        Debug.Log("success WhereIsMySynResult");
        int i = 0;
        Vector3 old = transform.Find("Image").localPosition;
        while (i < 3)
        {
            transform.Find("Image").localPosition = Vector3.right * 0.1f;
            yield return new WaitForSeconds(0.05f);
            transform.Find("Image").localPosition = Vector3.left * 0.1f;
            yield return new WaitForSeconds(0.05f);
            i++;
        }
        transform.Find("Image").localPosition = old;

    }
}
