using UnityEngine;
using cfg;

public class Slot : MonoBehaviour
{
    public Item item;
    public GameObject slotImage;
    public Sprite imageNull;
    public int slotId;

    void Update()
    {
        if (item == null)
        {
            slotImage.GetComponent<SpriteRenderer>().sprite = imageNull;
        }
        else
        {
            // slotImage.GetComponent<SpriteRenderer>().sprite = AssetDatabase.LoadAssetAtPath<Sprite>($"Assets/Images/Items/{item.ImageNormal}.png");
            slotImage.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>($"Images/Items/{item.ImageNormal}");
        }
    }


}
