using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slots : MonoBehaviour
{
    public List<Slot> slotsList = new List<Slot>();
    void OnDrawGizmos()
    {
        int a = 0;
        slotsList.Clear();
        foreach (Transform child in transform)
        {
            slotsList.Add(child.gameObject.GetComponent<Slot>());
            child.GetComponent<Slot>().slotId = a;
            a++;
        }
    }
}
