using System.Collections.Generic;
using UnityEngine;
using cfg;
using utility;
using System.Linq;
using System;
using Mono.Cecil;
using UnityEngine.UI;
using MyUI;

namespace synthesis
{

    public class SynthesisManager : MonoBehaviour
    {

        public GameObject synthesisMain;
        Button synthesisButton;
        public MyCursor myCursor;
        public Slots slots;
        public GameObject synResult;
        public static Item currentItem;//当前拿起来的物品
        public Slot nearestSlot;
        List<Item> _slotItems = new List<Item>();

        void Awake()
        {
            synthesisButton = synthesisMain.transform.Find("Button").GetComponent<Button>();
        }

        void Start()
        {
            CheckSynthesis();
        }

        void Update()
        {
            // Debug.Log("当前拿起来的物品是：" + currentItem?.TextName);
            if (Input.GetMouseButtonUp(0))//抬起左键（松手 ）时
            {
                Cursor.visible = false;//每次点击就隐藏鼠标
                CheckSlotItem();
            }
        }
        /// <summary>
        /// 检测slot要展示什么物品，因为我这做了场景UI和画布UI所以没法直接用eventtrigger(应该)
        /// </summary>
        void CheckSlotItem()
        {
            float nearestDistance = float.MaxValue;
            foreach (Slot slot in slots.slotsList)
            {
                float dis = Vector2.Distance(Input.mousePosition, Camera.main.WorldToScreenPoint(slot.transform.position));
                //Debug.Log(dis);
                if (dis < nearestDistance)
                {
                    nearestSlot = slot;
                    nearestDistance = dis;
                }
            }
            if (nearestDistance < 160)
            {
                //好，找到玩家点击的格子了
                //格子上的东西先还了，然后再把curItem给减掉
                UIManager.inventoryManager.ChangeInventoryDict(nearestSlot.item, 1);
                //inventoryManager.ChangeInventoryDict(currentItem, -1);
                //一旦slotItem的值改变了就要刷新背包
                nearestSlot.item = currentItem;
                Debug.Log("点击的是：" + nearestSlot.item?.TextName);
            }
            if (nearestSlot.item == null)
            {
                UIManager.inventoryManager.ChangeInventoryDict(currentItem, 1);
            }

            // if (currentItem == null)
            // {
            //     Debug.Log("没有拿任何东西");
            //     return;
            // }
            CheckSynthesis();
            currentItem = null;
            myCursor.SetMyCursor(null);
        }


        void ClearSlots()
        {
            foreach (var i in slots.slotsList)
            {
                UIManager.inventoryManager.ChangeInventoryDict(i.item, 1);
                i.item = null;
            }
            return;
        }


        /// <summary>
        /// 开始合成
        /// </summary>
        public void GoTrySynthesis()
        {
            CheckSynTable(out var targetItem, out var require);

            if (targetItem == null)
            {
                Debug.LogError("合成失败");
            }
            else
            {
                UserData.ChangeUserData(targetItem, 1);
                require.ForEach(n => UserData.ChangeUserData(n, -1));
                Debug.Log($"合成成功，目标物品：{targetItem.TextName},数量：{UserData.UserItems[targetItem]}");
            }
            ClearSlots();//这个方法会导致合成后的结果变成null*2
            synResult.GetComponent<Result>().ShowSynResult(targetItem);
            // Debug.Log($"传入的合成目标：{targetItem?.TextName}");
        }

        /// <summary>
        /// 检测合成相关（目前是检测合成按钮的状态）
        /// </summary>
        public void CheckSynthesis()
        {
            RecreateSlotItemList();
            synthesisButton.interactable = _slotItems.Count > 0 ? true : false;
            CheckSynTable(out var targetItem, out var require);//这个方法会导致合成后的结果变成null
            synResult.GetComponent<Result>().PreshowSynResult(targetItem);
        }

        void CheckSynTable(out Item target, out List<Item> require)
        {
            target = null;
            require = null;
            //思路是这样的：首先我们是无序的合成表ABC等价于ACB
            //1.先获得一个list拥有所有格子里的id
            //2.遍历合成表，判断每个合成公式里是否contains第一点的list的第一个
            //3.从满足的合成公式里去判断第二个(递归)，以此类推
            //4.对于多出的部分，需要返还多出的材料
            //5.对于更优先的部分，比如2个碎银合一个银块，3个碎银合一个超级银块，优先材料多的
            //6.不用匹配没达到要求材料数量的部分
            //======以下为补充内容======
            //7.我放入了123，我有1+2=3、1+3=4、2+3=5，那合成的话会出来哪个呢？让玩家自己选？或者必须严格照公式来，1+2+3就返回错误
            //8.就照着严格按照公式来吧，也免得麻烦
            RecreateSlotItemList();

            var _synTableList = Utilities.tables.TbSynthesisTable.DataList;
            //遍历整个合成表的所有字段
            //遍历每一个合成公式
            foreach (var syntab in _synTableList)
            {
                //只判断完全等于我投入的数量的，减少循环量
                if (syntab.RequireItem.Count == _slotItems.Count)
                {
                    var x = new List<Item>();
                    x = syntab.RequireItem_Ref;
                    x.Sort();
                    bool isEqual = false;
                    isEqual = Enumerable.SequenceEqual(_slotItems, x);
                    if (isEqual)
                    {
                        target = syntab.TargetItem_Ref;
                        require = _slotItems;
                        return;
                    }
                }
            }
            return;
        }

        /// <summary>
        /// 重新检查所有格子现在都有什么物体
        /// </summary>
        void RecreateSlotItemList()
        {
            _slotItems.Clear();
            slots.slotsList.ForEach(n =>
            {
                if (n.item != null)
                {
                    _slotItems.Add(n.item);
                }
            }
            );
            _slotItems.Sort();

        }


        public void Temp()
        {
            Item item = Utilities.tables.TbItem.Get(3);
            UserData.ChangeUserData(item, 100);
            UIManager.inventoryManager.RefreshInventoryByInventoryDict();
        }


    }

}
