using UnityEngine;

public class UIPositionBinding : MonoBehaviour
{
    [Header("\n================本物体的坐标已被脚本控制================\n")]
    public GameObject targetGameObject;
    public Vector2 offset;
    void Start()
    {
        transform.position = offset + (Vector2)Camera.main.WorldToScreenPoint(targetGameObject.transform.position);

    }
}
