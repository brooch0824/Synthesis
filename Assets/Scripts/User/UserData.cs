using System.Collections.Generic;
using cfg;
using Unity.VisualScripting;
using utility;

public static class UserData
{
    public static Dictionary<Item, int> UserItems { get; private set; } = new Dictionary<Item, int>();
    public static void RegisterUserData()
    {
        UserItems.Clear();
        // //将UserInitial里的数据赋予玩家作为初始值
        // foreach (var i in Utilities.tables.TbUserInitial.DataList)
        // {
        //     foreach (var j in i.Items)
        //     {
        //         Item item = j.Id_Ref;
        //         int number = j.Number;
        //         UserItems.Add(item, number);
        //     }
        // }

        foreach (var item in Utilities.tables.TbItem.DataList)
        {
            UserItems.Add(item, 0);
        }
        foreach (var itemNum in Utilities.tables.TbUserInitial.DataList)
        {
            itemNum.Items.ForEach(n => UserItems[n.Id_Ref] = n.Number);
        }
        // foreach (var i in UserItems)
        // {
        //     UnityEngine.Debug.Log($"物品ID:{i.Key.TextName},数量：{i.Value}");
        // }

    }


    /// <summary>
    /// 改变用户数据
    /// </summary>
    /// <param name="data"></param>
    /// <typeparam name="T"></typeparam>
    public static void ChangeUserData<T>(T data, int number)
    {
        if (data == null) return;
        //如果要改变单个物体
        if (typeof(T) == typeof(Item))
        {
            if (UserItems.ContainsKey(data.ConvertTo<Item>()))
            {
                UserItems[data.ConvertTo<Item>()] += number;
            }
        }
    }

    public static void ChangeUserData<T>(T data)
    {
        if (data == null) return;
        //如果要刷新数据列表
        if (typeof(T) == typeof(Dictionary<Item, int>))
        {
            UserItems = data.ConvertTo<Dictionary<Item, int>>();
        }
    }
}
