set LUBAN_DLL=..\..\luban\Luban\Luban.dll
set CONF_ROOT=_Excels

dotnet %LUBAN_DLL% ^
    -t client ^
    -c cs-simple-json ^
    -d json  ^
    --conf luban.conf ^
    -x outputCodeDir=..\Assets\Output\CSharp ^
    -x outputDataDir=..\Assets\Output\Json

pause